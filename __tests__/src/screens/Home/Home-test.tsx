// /**
//  * @format
//  */
import React from 'react';
import Enzyme, {shallow, mount, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import Home from 'src/screens/Home/Home';
Enzyme.configure({adapter: new Adapter()});
import {fetchDataService} from 'services/fetchData';
jest.mock('services/fetchData');
describe('All Tests ', () => {
  let wrapper;
  beforeAll(() => {
    fetchDataService.mockImplementation(() => {
      return {data: [{titile: '', price: 0, img: ''}]};
    });
  });
  beforeEach(() => {
    wrapper = shallow(<Home />);
  });

  describe('HOME SCREEN ', () => {
    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
    it('renders correctly', () => {
      expect(wrapper.find({testID: 'loadData'}).length).toBe(1);
    });
    it('renders correctly', () => {
      expect(wrapper.find({testID: 'Filter'}).length).toBe(0);
    });

    it('renders correctly', () => {
      wrapper.setState({showFilter: true});
      expect(wrapper.find({testID: 'Filter'}).length).toBe(1);
    });
    it('renders correctly', () => {
      wrapper.setState({loading: false});
      expect(wrapper.find({testID: 'loading'}).length).toBe(0);
    });
    it('errorLoad correctly', () => {
      wrapper.setState({errMsg: ''});
      expect(wrapper.find({testID: 'errorLoad'}).length).toBe(0);
    });
    it('TryAgain renders', () => {
      wrapper.setState({errMsg: 'error found'});
      expect(wrapper.find({testID: 'errorLoad'}).length).toBe(1);
      let mockFn = jest.fn();
      wrapper.instance().reloadData = mockFn;
      wrapper.find('TryAgain').props().onPress();
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
    it('Button', () => {
      wrapper.find('Button').props().onPress();
      expect(wrapper.find({testID: 'Filter'}).length).toBe(1);
    });
    it('ActivityIndicator rendering', () => {
      wrapper.setState({loading: true});
      expect(wrapper.find('ActivityIndicator')).toHaveLength(1);
    });

    it('ActivityIndicator rendering', () => {
      wrapper.setState({loading: false});
      expect(wrapper.find('ActivityIndicator')).toHaveLength(0);
    });
  });
});

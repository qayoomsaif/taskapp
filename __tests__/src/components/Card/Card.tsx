// /**
//  * @format
//  */
import React from 'react';
import Enzyme, {shallow, mount, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import {Card} from 'src/components/Card/Card';
Enzyme.configure({adapter: new Adapter()});
import {fetchDataService} from 'services/fetchData';
describe('All Tests ', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(
      <Card
        data={{title: 'name', price: 12, img: ''}}
        onPressQty={jest.fn()}
      />,
    );
  });
  describe('Card', () => {
    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
    it('Text', () => {
      expect(wrapper.find('Text')).toHaveLength(6);
    });
    it('onPress sun', () => {
      let mockFn = jest.fn();
      wrapper.instance().subQty = mockFn;
      wrapper.find({testID: 'sub'}).props().onPress();
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
    it('onPress add', () => {
      let mockFn = jest.fn();
      wrapper.instance().addQty = mockFn;
      wrapper.find({testID: 'add'}).props().onPress();
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
    it('onPress remove', () => {
      let mockFn = jest.fn();
      wrapper.instance().removeQty = mockFn;
      wrapper.find({testID: 'remove'}).props().onPress();
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
  });
});

import React, {Component} from 'react';
import {
  View,
  FlatList,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import styles from './Home.styles';

import {Card, Button, TryAgain} from 'components';
import {fetchDataService} from 'services/fetchData';
interface itemProp {
  item: any;
}
interface State {
  totalPrice: number;
  filterData: Array;
  selectedFilter: string;
  showFilter: boolean;
  loadedData: any;
  loading: boolean;
  errMsg: any;
}

export default class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      totalPrice: 0,
      showFilter: false,
      selectedFilter: 'All',
      filterData: ['All', 'Black', 'Red', 'Stone'],
      errMsg: null,
      loading: false,
      loadedData: [],
    };
  }
  onDecrement = () => {
    this.dropdown.show();
  };
  async fetch() {
    return await fetchDataService();
  }
  async fetchData(type: string) {
    this.setState({loading: true, errMsg: '', loadedData: []});
    try {
      const response = await this.fetch();
      if (response.data) {
        if (type != 'All') {
          let data = response.data;
          let result = data.filter((item) => item.colour == type);
          this.setState({loading: false, errMsg: '', loadedData: result});
          return;
        }
        this.setState({loading: false, errMsg: '', loadedData: response.data});
      }
    } catch (e) {
      this.setState({loading: true, errMsg: e.message, loadedData: []});
    }
  }
  componentDidMount() {
    const {selectedFilter} = this.state;
    this.fetchData(selectedFilter);
  }
  setTotalPrice(price: number) {
    this.setState({totalPrice: this.state.totalPrice + price});
  }
  selectingFilter(type: string) {
    const {selectedFilter} = this.state;
    if (selectedFilter != type) {
      this.fetchData(type);
      this.setState({showFilter: false, selectedFilter: type, totalPrice: 0});
      return;
    }
    this.setState({showFilter: false});
  }
  reloadData() {
    const {selectedFilter} = this.state;
    this.fetchData(selectedFilter);
  }
  render() {
    const {
      totalPrice,
      filterData,
      selectedFilter,
      showFilter,
      loadedData,
      loading,
      errMsg,
    } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Button
            buttonText={selectedFilter}
            avatar={require('src/assets/icons/dropDown.png')}
            onPress={() => this.setState({showFilter: !showFilter})}
            accessibilityLabel="decrement"
            color="red"
          />
        </View>
        <FlatList
          style={styles.flatListBlock}
          testID="loadData"
          data={loadedData}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}: itemProp) => {
            return (
              <Card
                testID="Card"
                onPressQty={(price) => this.setTotalPrice(price)}
                data={item}
              />
            );
          }}
        />
        <View style={styles.totalBlock}>
          <Text style={styles.totalText}>{'€' + totalPrice}</Text>
        </View>
        {showFilter ? (
          <View style={styles.filterDropDwon}>
            <FlatList
              data={filterData}
              testID="Filter"
              keyExtractor={(i) => i}
              renderItem={({item}: itemProp) => {
                return (
                  <TouchableOpacity
                    onPress={() => this.selectingFilter(item)}
                    style={styles.filterItemBlock}>
                    <Text>{item}</Text>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        ) : null}
        {loading ? (
          <View testID="loading" style={styles.loading}>
            <ActivityIndicator
              style={styles.styleComponent}
              color={'grey'}
              size={'large'}
            />
          </View>
        ) : null}
        {errMsg ? (
          <TryAgain
            testID="errorLoad"
            onPress={() => this.reloadData()}
            show={true}
            msg={errMsg}
          />
        ) : null}
      </View>
    );
  }
}

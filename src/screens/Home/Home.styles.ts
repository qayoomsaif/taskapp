import {StyleSheet} from 'react-native';
import {colors, resize} from 'constantsStyle';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.containerBg,
  },
  loadingFooter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    marginVertical: resize.heightScale(20),
    marginLeft: resize.widthScale(20),
    height: resize.heightScale(50),
    width: resize.width / 2,
    justifyContent: 'center',
  },
  filterDropDwon: {
    position: 'absolute',
    top: resize.heightScale(60),
    left: resize.widthScale(20),
    width: resize.width / 3,
    borderWidth: 1,
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
  filterItemBlock: {
    paddingHorizontal: resize.widthScale(20),
    paddingVertical: resize.heightScale(10),
    borderBottomWidth: 0.5,
  },
  totalBlock: {
    position: 'absolute',
    height: resize.heightScale(45),
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: colors.blue,
  },
  totalText: {
    textAlign: 'center',
    color: colors.white,
    width: resize.width / 4 + resize.widthScale(20),
    fontSize: resize.square(20),
  },
  flatListBlock: {
    flex: 1,
    marginBottom: resize.heightScale(45),
  },
  dropdownText: {
    fontSize: resize.square(20),
  },
  loading: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;

export const colors = {
  primary: '#3F51B5',
  accent: '#AF2923',
  containerBg: '#FFF',
  textColor: '#000',
  borderColor: '#ccc',
  white: '#FFFFFF',
  greyLight: '#F7F7F7',
  greyDark: '#808080',
  orange: '#FFBA00',
  mainBlack: '#322941',
  lightGrey: '#ADA9B3',
  blue: '#466CE4',
};

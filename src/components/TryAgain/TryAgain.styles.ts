import {StyleSheet} from 'react-native';
import {colors, resize} from 'constantsStyle';

export default styles = StyleSheet.create({
  maincontainer: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.white,
  },
  clodIcon: {
    width: resize.widthScale(200),
    height: resize.heightScale(100),
  },
  msgText: {
    marginVertical: resize.heightScale(30),
    marginHorizontal: resize.widthScale(30),
    textAlign: 'center',
    color: colors.mainBlack,
    marginTop: resize.heightScale(25),
    fontSize: resize.square(16),
  },
  buttonBlock: {
    paddingHorizontal: resize.widthScale(30),
    height: resize.heightScale(45),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: resize.heightScale(5),
    backgroundColor: colors.blue,
  },
  buttonText: {
    color: colors.white,
    fontSize: resize.square(16),
    paddingBottom: resize.heightScale(3),
  },
});

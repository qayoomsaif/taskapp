import React, {Component} from 'react';
import {View, Text, TouchableOpacity, ViewProps, Image} from 'react-native';
import styles from './TryAgain.styles';
interface Props extends ViewProps {
  msg: string;
  show: boolean;
  buttonText: string;
  icon: any;
  style: any;
  onPress?: () => void;
}
export class TryAgain extends Component<Props, {}> {
  render() {
    const { msg, show, buttonText, icon, style, onPress} = this.props;
    return show ? (
      <View {...this.props} style={[style ? style : styles.maincontainer]}>
        <Image
          style={styles.clodIcon}
          source={icon ? icon : require('src/assets/icons/cloud.png')}
          resizeMode={'contain'}
        />
        <Text style={styles.msgText}>
          {msg || 'Something went to wrong please try again later'}
        </Text>
        <TouchableOpacity
          onPress={() => (onPress ? onPress() : null)}
          style={styles.buttonBlock}>
          <Text style={styles.buttonText}>
            {buttonText ? buttonText : 'Try again'}
          </Text>
        </TouchableOpacity>
      </View>
    ) : null;
  }
}

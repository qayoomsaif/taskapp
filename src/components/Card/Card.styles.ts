import {StyleSheet} from 'react-native';
import {colors, resize} from 'constantsStyle';

export default styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    backgroundColor: colors.containerBg,
    borderBottomWidth: 1,
    borderBottomColor: colors.borderColor,
    paddingVertical: resize.heightScale(12),
  },
  avatarStyle: {
    marginHorizontal: resize.widthScale(10),
    width: resize.width / 4 - resize.widthScale(10),
    height: resize.width / 4 - resize.widthScale(10),
    resizeMode: 'cover',
    backgroundColor: colors.borderColor,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: resize.square(5),
  },
  centerBlock: {
    width: resize.width / 2 - resize.widthScale(40),
  },
  titleText: {
    fontSize: resize.square(14),
    fontWeight: '600',
    color: colors.textColor,
  },
  priceText: {
    marginTop: resize.heightScale(10),
    fontSize: resize.square(16),
    fontWeight: '800',
    color: colors.textColor,
    textAlign: 'right',
  },
  rightBlock: {
    width: resize.width / 4 + resize.widthScale(20),
    height: resize.width / 4 - resize.widthScale(20),
    marginHorizontal: resize.heightScale(10),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  removeBlock: {
    alignItems: 'center',
  },
  addSubBlock: {
    marginHorizontal: resize.widthScale(5),
    paddingVertical: resize.heightScale(5),
  },
  removeText: {
    fontSize: resize.square(12),
  },
  addSubText: {
    fontSize: resize.square(20),
  },
});

import React, {Component} from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  Image,
  TouchableOpacityProps,
} from 'react-native';
import styles from './Button.styles';
interface Props extends TouchableOpacityProps {
  buttonText: string;
  avatar: string;
}

export class Button extends Component<Props, {}> {
  render() {
    const {buttonText, avatar} = this.props;
    return (
      <TouchableOpacity {...this.props} style={styles.buttonBlock}>
        <Text style={styles.buttonText}>{buttonText}</Text>
        <Image style={styles.buttonIcon} resizeMode="contain" source={avatar} />
      </TouchableOpacity>
    );
  }
}

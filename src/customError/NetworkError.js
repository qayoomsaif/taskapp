
module.exports = class NetworkError extends require('./UserError') {
    constructor(errors, type) {
        // Overriding both message and status code.
        super(errors, type);
        // Saving custom property.
        this.mainType = 'NetworkError'
        this.type = type || 'NetworkError'

        this.messageApi = errors.messageApi ? errors.messageApi : 'error not found'
        this.url = errors.url ? errors.url : null
        this.status = errors.status ? errors.status : 999
        this.message = 'Please check your internet connection and try again';
    }
};

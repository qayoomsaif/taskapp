
import ExtendableBuiltin from './ExtendableBuiltin'
module.exports = class ApiError extends ExtendableBuiltin(Error) {
  constructor(errors, type) {
    super(errors, type);
    this.mainType = 'ApiError'
    this.type = type || ApiError.name;
    this.message = errors.message ? errors.message : 'Something went wrong. Please try again.'
    this.messageApi = errors.messageApi ? errors.messageApi : 'error not found'
    this.url = errors.url ? errors.url : null
    this.status = errors.status ? errors.status : 111
    this.object = errors.object ? errors.object : { message: 'Something went wrong. Please try again.', detail: {} }
    this.detail = errors.object ? errors.object.detail ? errors.object.detail : {} : {}
    return
  }
};

import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import Home from 'screens/Home/Home';

const Routes = () => {
  return (
    <Router>
      <Scene key="root">
        <Scene
          key="Home"
          component={Home}
          hideNavBar
          panHandlers={null}
          initial
        />
      </Scene>
    </Router>
  );
};

module.exports = {Routes};
